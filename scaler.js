import {
  StyleSheet,
  Platform,
  Dimensions,
} from 'react-native'

import Config from 'react-native-config'

const PROPERTIES = [
  'borderRadius',
  'borderWidth',
  'borderLeftWidth',
  'borderRightWidth',
  'borderTopWidth',
  'borderBottomWidth',
  'width',
  'height',
  'maxWidth',
  'minWidth',
  'maxHeight',
  'minHeight',
  'top',
  'right',
  'bottom',
  'left',
  'fontSize',
  'lineHeight',
  'letterSpacing',
  'padding',
  'paddingVertical',
  'paddingHorizontal',
  'paddingTop',
  'paddingRight',
  'paddingBottom',
  'paddingLeft',
  'margin',
  'marginVertical',
  'marginHorizontal',
  'marginTop',
  'marginRight',
  'marginBottom',
  'marginLeft',
  'shadowRadius',
]

export const MIN = true
export const MAX = false

export const iPhoneXHomeBarHeight = 34

export function createScaledStylesWithTests(styles, expectedStyles, phoneAxis, largerScreenSize, tabletScreenSize = null, phoneScreenSize = null) {
  const axis = isPhone() ? phoneAxis : MIN

  const baseScreenSize = _getSize(largerScreenSize, tabletScreenSize, phoneScreenSize)

  const scalingFactor = _calculateScalingFactor(axis, baseScreenSize)

  const newStyles = _parseStyles(styles, scalingFactor, expectedStyles)

  return StyleSheet.create(newStyles)
}

export function createScaledStyles(styles, phoneAxis, largerScreenSize, tabletScreenSize = null, phoneScreenSize = null) {
  const axis = isPhone() ? phoneAxis : MIN

  const baseScreenSize = _getSize(largerScreenSize, tabletScreenSize, phoneScreenSize)

  const scalingFactor = _calculateScalingFactor(axis, baseScreenSize)

  const newStyles = _parseStyles(styles, scalingFactor)

  return StyleSheet.create(newStyles)
}

function _getSize(largeSize, tabletSize, phoneSize) {
  if (isPhone()) {
    if (phoneSize !== null) {
      return phoneSize
    }
    if (tabletSize !== null) {
      return tabletSize
    }
  }
  if (tabletSize !== null && !(isPhone() || isXlTablet()) && !isV2()) {
    return tabletSize
  }
  return largeSize
}

function _getDeviceScreenSize(minAxis = true) {
  const screenSizes = Dimensions.get('screen')
  const min = Math.min(screenSizes.height, screenSizes.width)
  const max = Math.max(screenSizes.height, screenSizes.width)
  const screenSize = minAxis ? min : max

  if (!isV2() && Platform.OS === 'android' && ((minAxis && min === screenSizes.height) || (!minAxis && max === screenSizes.height))) {
    return _calculateAndroidPhoneHeight()
  }

  return screenSize
}

function _calculateAndroidPhoneHeight() {
  const windowSizes = Dimensions.get('window')
  const windowHeight = windowSizes.height

  const screenSizes = Dimensions.get('screen') ?? undefined

  if (!screenSizes)
    return windowHeight
  const screenHeight = screenSizes.height

  // When the navigation bar is horizontal (bottom)
  if (screenHeight - windowHeight !== 0) {
    return windowHeight
  }

  const windowWidth = windowSizes.width
  const screenWidth = screenSizes.width

  // When the navigation bar is vertical (either on the left or right)
  if (screenWidth - windowWidth !== 0)
    return (screenHeight * windowWidth) / screenWidth
}

function _calculateScalingFactor(axis, baseScreenSize) {
  const screenSize = _getDeviceScreenSize(axis)

  return (screenSize / baseScreenSize)
}

function _parseStyles(styles, scalingFactor, expectedStyles = null) {
  const styleMap = new Map(Object.entries(styles))
  const expectedStyle = (key) => expectedStyles === null ? null : expectedStyles[key]

  let parsedStyles = {}

  styleMap.forEach((value, key) => {
    Object.defineProperty(parsedStyles, key, {
      value: _parseStyle(value, scalingFactor, key, expectedStyle(key)),
    })
  })

  return parsedStyles
}

function _parseStyle(style, scalingFactor, styleKey, expectedStyle = null) {
  let parsedStyle = style
  let failsTest = false

  for (const prop of PROPERTIES) {
    const propertyValue = style[prop]
    const expectedSize = expectedStyle === null ? null : expectedStyle[prop]
    let size = propertyValue
    if (propertyValue && typeof propertyValue === 'object') {
      size = _parseSize(propertyValue, scalingFactor)
      parsedStyle[prop] = size
    }
    if (expectedSize !== null) {
      if (Math.abs(size - expectedSize) > 0.01) {
        failsTest = true
        console.error(`[SCALER - TEST FAIL] ${styleKey}: ${prop} value is wrong. Expected ${expectedSize}, but got ${size}.`)
      } else {
        console.log(`[SCALER - TEST PASS] ${styleKey}: ${prop} value is correct.`)
      }
    }
  }

  return parsedStyle
}

function _parseSize(elementSizeRules, scalingFactor) {
  const {
    large,
    tablet,
    phone,
  } = elementSizeRules

  const elementSize = _getSize(large, tablet, phone)
  const min = elementSizeRules.min
  const max = elementSizeRules.max
  const scaledElementSize = elementSize * scalingFactor

  if (min && min > scaledElementSize) {
    return min
  }

  if (max && max < scaledElementSize) {
    return max
  }

  return scaledElementSize
}


/**
 * Helper function that is used when the element size scales depending on the current device type.
 * 
 * The default starting scaling value is the one used for xl tablets.
 * 
 * **Note:** If you need to provide more than on minimum or maximum element sizes, use the **'useFixedSize'** helper function like this:
 * 
 * **useScreenRatio(12, 8, 4, useFixedSize(10, 6, 2))**
 * 
 * The example above restricts the minimum element size to 10, on xl tablets, 6 on smaller tablets and 2 on phones.
 * 
 * @param {*} largerElementSize - starting scaling value to be used when the current device type is xl tablet .
 * @param {*} tabletElementSize - starting scaling value to be used when the current device type is smaller tablet.
 * @param {*} phoneElementSize - starting scaling value to be used when the current device type is phone.
 * @param {*} minElementSize - minimum value to be returned to prevent under scaling.
 * @param {*} maxElementSize - maximum value to be returned to prevent over scaling.
 */
export function useScreenRatio(largerElementSize, tabletElementSize = null, phoneElementSize = null, minElementSize = null, maxElementSize = null) {
  return {
    large: largerElementSize,
    tablet: tabletElementSize,
    phone: phoneElementSize,
    min: minElementSize,
    max: maxElementSize,
  }
}

function _useFixedSize(largerElementSize, tabletElementSize = null, phoneElementSize = null) {
  return _getSize(largerElementSize, tabletElementSize, phoneElementSize)
}

/**
 * Helper function that is used when the element size does not scale, but instead has a fixed value when running on different device types.
 * 
 * The default value returned is the one used for xl tablets.
 * 
 * @param {*} largerElementSize - value to be returned when the current device type is xl tablet .
 * @param {*} tabletElementSize - value to be returned when the current device type is smaller tablet.
 * @param {*} phoneElementSize - value to be returned when the current device type is phone.
 * @param {*} v2Size - value to be returned when the current device type is v2.
 */
export function useFixedSize(xlTabletElementSize, tabletElementSize = null, phoneElementSize = null, v2Size = null) {
  if (isV2()) {
    if (v2Size !== null) {
      return typeof v2Size === 'number' ? useScreenRatio(v2Size) : v2Size
    }
    return typeof xlTabletElementSize === 'number' ? useScreenRatio(xlTabletElementSize) : xlTabletElementSize
  } else {
    return _useFixedSize(xlTabletElementSize, tabletElementSize, phoneElementSize)
  }
}

/**
 * Helper function that is used when a size based on a percentage of the screen dimensions is needed.
 * 
 * @param {*} percentage - percentage value from 0 to 100.
 * @param {*} useScreenHeight - flag to define if the percentage is calculated based on the screen height or the screen width. Default is screen height.
 */
export function useScreenPercentage(percentage, useScreenHeight = true) {
  const screenHeight = Dimensions.get('screen').height
  const screenWidth = Dimensions.get('screen').width

  const ratio = percentage / 100

  const size = useScreenHeight ? screenHeight : screenWidth

  return size * ratio
}

/**
 * Helper function that detects if the current device is a phone.
 * 
 */
export function isPhone() {
  const screenSizes = Dimensions.get('screen')
  let min = Math.min(screenSizes.width, screenSizes.height)
  if (Platform.OS === 'android') {
    const windowSizes = Dimensions.get('window')
    min = Math.min(windowSizes.width, _calculateAndroidPhoneHeight())
  }
  return (min <= 600)
}

/**
 * Helper function that detects if the current device is a XL Tablet.
 * 
 * **Note: In order to detect if a device is a small tablet, you need to use this condition: !(isXlTablet() || isPhone())**
 */
export function isXlTablet() {
  const size = Dimensions.get('screen')
  const min = Math.min(size.width, size.height)
  const max = Math.max(size.width, size.height)
  return (max * min >= 1100000) || isV2()
}

/**
 * Helper function to detect if the current device uses the new iOS home bar
 * 
 */
export function isIphoneXorAbove() {
  const size = Dimensions.get('screen')
  const min = Math.min(size.width, size.height)
  const max = Math.max(size.width, size.height)

  return (
    Platform.OS === 'ios' && (
      (max === 896 && min === 414) ||
      (max === 812 && min === 375)) ||
    (min === 1024 && max === 1366)
  )
}

/**
 * Helper function that returns a value depending on the platform of the current device.
 * 
 * **Note: The type of the value can be any one, like number, string, object, component, etc.**
 * 
 * @param {*} iosValue - value to be returned when the current platform is iOS
 * @param {*} androidValue - value to be returned when the current platform is android
 */
export function usePlatform(iosValue, androidValue = null) {
  if (Platform.OS === 'android' && androidValue) {
    return androidValue
  }
  return iosValue
}

/**
 * Helper function that returns the expected element sizes on known devices.
 * 
 * Returns the expected element size if the current device is a known device, othewise returns null.
 * 
 * @param {*} elementSizeOnV2 - expected value to be returned when the current device has the same resolution as the V2
 * @param {*} elementSizeOnIpadPro - expected value to be returned when the current device has the same resolution as an iPad Pro
 * @param {*} elementSizeOnIPad - expected value to be returned when the current device has the same resolution as an iPad 5th generation
 * @param {*} elementSizeOnIPhone11 - expected value to be returned when the current device has the same resolution as an iPhone 11
 * @param {*} elementSizeOnIphone8 - expected value to be returned when the current device has the same resolution as an iPhone 8
 * @param {*} elementSizeOnIPhoneSE - expected value to be returned when the current device has the same resolution as an iPhone SE
 * 
 */
export function testSizes(elementSizeOnV2, elementSizeOnIpadPro, elementSizeOnIPad, elementSizeOnIPhone11, elementSizeOnIphone8, elementSizeOnIPhoneSE) {
  const screenDimensions = Dimensions.get('screen')
  const min = Math.min(screenDimensions.height, screenDimensions.width)
  const max = Math.max(screenDimensions.height, screenDimensions.width)

  if (max === 1280 && min === 720 && elementSizeOnV2) {
    return elementSizeOnV2 * (720 / 1080)
  }

  if (max === 1366 && min === 1024 && elementSizeOnIpadPro) {
    return elementSizeOnIpadPro
  }

  if (max === 1024 && min === 768 && elementSizeOnIPad) {
    return elementSizeOnIPad
  }

  if (max === 896 && min === 414 && elementSizeOnIPhone11) {
    return elementSizeOnIPhone11
  }

  if (max === 667 && min === 375 && elementSizeOnIphone8) {
    return elementSizeOnIphone8
  }

  if (max === 568 && min === 320 && elementSizeOnIPhoneSE) {
    return elementSizeOnIPhoneSE
  }

  return null
}

/**
 * Helper function used to check if the current device is a V2 Monitor.
 * 
 * Returns true if the current device is the V2 Monitor.
 * 
 */
export function isV2() {
  return Config.RN_V2_ONLY === 'true'
}

/**
 * Helper function used to obtain scaled sizes outside of styling sheets.
 * 
 */
export function getScreenRatioSize(phoneAxis, largeScreenSize, tabletScreenSize, phoneScreenSize, largeSize, tabletSize = null, phoneSize = null, min = null, max = null) {
  const baseScreenSize = _getSize(largeScreenSize, tabletScreenSize, phoneScreenSize)
  const axis = isPhone() ? phoneAxis : MIN
  const scalingFactor = _calculateScalingFactor(axis, baseScreenSize)
  const elementSize = _getSize(largeSize, tabletSize, phoneSize)
  const scaledElementSize = elementSize * scalingFactor
  if (min && min > scaledElementSize) {
    return min
  }
  if (max && max < scaledElementSize) {
    return max
  }
  return scaledElementSize
}

/**
 * Helper function used to check if the device is running on landscape.
 * 
 * Returns true if the current screen orientation is landscape, false if portrait.
 * 
 */
export function isLandscape() {
  const dim = Dimensions.get('screen')
  const screenHeight = dim.height
  const screenWidth = dim.width

  return screenWidth > screenHeight
}
